Install-Module -Name HNS -Confirm:$False -Force -AllowClobber
Install-Module -Name ThreadJob -Confirm:$False -Force  -AllowClobber
git clone https://github.com/microsoft/containers-toolkit.git C:\ProgramData\containers-toolkit

$env:PSModulePath = ('{0};C:\ProgramData\containers-toolkit' -f $env:PSModulePath)
[Environment]::SetEnvironmentVariable('PSModulePath', ('{0};C:\ProgramData\containers-toolkit' -f $env:PSModulePath), [EnvironmentVariableTarget]::Machine)
Import-Module -Name containers-toolkit -Force
Install-ContainerTools -Confirm:$False -Force
