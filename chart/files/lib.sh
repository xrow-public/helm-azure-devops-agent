#!/bin/bash

unregister() {
  write_header "Unregister, removing agent from server. The signal was ${1}."

  # If the agent has some running jobs, the configuration removal process will fail ; so, give it some time to finish the job
  while true; do
    # If the agent is removed successfully, exit the loop
    bash ./config.sh remove \
        --auth PAT \
        --token "$AZP_TOKEN" \
        --unattended \
      && break
    echo "Retrying in 15 secs"
    sleep 15
  done
  output_logs
}

write_header() {
  lightcyan='\033[1;36m'
  nocolor='\033[0m'
  echo -e "${lightcyan}➡️ $1${nocolor}"
}

output_logs() {
  write_header "Printing agent diag logs"
  cat $AGENT_DIAGLOGPATH/*.log
}