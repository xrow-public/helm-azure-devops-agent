# Azure devops agent for AKS

The ideas of this agent are:

* Support AKS out of-the-box without additional overhead configuration
* Support KEDA and no other options.
* Enable **windows container** and **linux container** builds via the rootless runtime in the container. 
* Reduce configutation options to a minium.

Note: Using the host containered runtime gives performance improvements, but also security downfalls. Consider using a seperate node pool for agents.

## Install

* Setup a agent pool called **kuberentes** for your organisation. The name is customizable via the values.yaml.
* Install the charts

Create values.yaml / agent.yaml

```yaml
pipelines:
  organizationURL: https://dev.azure.com/xrowgmbh
  personalAccessToken: XXXXXXXXXXXXXXXXXXXXXX
  poolName: kubernetes
```

Execute helm

```bash
helm upgrade --install -n azure-devops-agent --create-namespace -f agent.yaml agent oci://registry.gitlab.com/xrow-public/helm-azure-devops-agent/charts/azure-devops-agent --version $CI_DOCUMENTATION_LATEST_TAG
```

## Example Pipeline

azure-pipelines.yml

```yaml
trigger:
- main

jobs:
- job: HelloLinux
  displayName: 'Hello Linux Job'
  pool:
    name: kubernetes
    demands:
    - Agent.OS -equals Linux
  steps:
  - script: |
      echo Hello, world from Job!
    displayName: 'Run a one-line script in Job'
- job: HelloWindows
  displayName: 'Hello Windows Job'
  pool:
    name: kubernetes
    demands:
    - Agent.OS -equals Windows_NT
  steps:
  - script: |
      echo Hello, world from Job!
    displayName: 'Run a one-line script in Job'
- job: NerdctlBuildLinux
  displayName: 'Linux Build Job'
  pool:
    name: kubernetes
    demands:
    - Agent.OS -equals Linux
  steps:
  - script: |
      echo "Hello, from Job: nerdctl build ./container/linux/"
      nerdctl build ./container/linux/
    displayName: 'Run script: nerdctl build ./container/linux/'

- job: NerdctlBuildWindows
  displayName: 'Windows Build Job'
  pool:
    name: kubernetes
    demands:
    - Agent.OS -equals Windows_NT
  steps:
  - script: |
      echo "Hello, from Job: nerdctl.exe build ./container/windows/"
      ctr i pull mcr.microsoft.com/windows/servercore:ltsc2022
      nerdctl build ./container/windows/
    displayName: 'Run script: nerdctl build ./container/windows/'
```

This agent was inspried by [Blue Agent](https://github.com/clemlesne/blue-agent)