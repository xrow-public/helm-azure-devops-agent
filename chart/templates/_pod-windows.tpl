{{ include "common.images.renderPullSecrets" ( dict "images" (list .Values.windows.image) "context" $) }}

serviceAccountName: {{ include "common.names.fullname" . }}
{{- if .Values.windows.automountServiceAccountToken }}
automountServiceAccountToken: true
{{- else }}
automountServiceAccountToken: false
{{- end }}
initContainers: {{ toYaml .Values.windows.initContainers | nindent 2 }}
terminationGracePeriodSeconds: 30
restartPolicy: Never
securityContext: {{ toYaml .Values.windows.podSecurityContext | nindent 4 }}
containers:
  {{- if .Values.windows.sidecarContainers }}
  {{- toYaml .Values.windows.sidecarContainers | trim | nindent 2 }}
  {{- end }}
  - name: azp-agent
    securityContext: {{ toYaml .Values.windows.securityContext | nindent 6 }}
    image: {{ include "common.images.image" ( dict "imageRoot" .Values.windows.image "global" .Values.global ) }}
    imagePullPolicy: {{ .Values.windows.image.pullPolicy }}
    command:
      - pwsh
    args:
      - -WorkingDirectory
      - C:\app-root\azp-agent
      - -File
      - C:\app-root\scripts\start.ps1
    startupProbe:
      failureThreshold: 1000
      periodSeconds: 10
      successThreshold: 1
      exec:
        command:
          - pwsh
          - -Command
          - |
            if (Get-Content C:\app-root\azp-logs\*.log | Select-String 'Running job') {
              Write-Output "Agent is running jobs";
              exit 0;
            }
            Write-Output "Agent is not running jobs";
            exit 1;
    livenessProbe:
      exec:
        command:
          - pwsh
          - -Command
          - |
            if (!(Test-Path "C:\\app-root\\azp-agent\\.agent")) {
              Write-Output "Agent is not running";
              Start-Sleep -Seconds 3;
              exit 1;
            }
            exit 0;
    lifecycle:
{{- if not .Values.pipelines.cache.volumeEnabled }}
      preStop:
        exec:
          command:
            - pwsh
            - -Command
            - |
              # For security reasons, force clean the pipeline workspace at restart -- Sharing data bewteen pipelines is a security risk
              Remove-Item -Recurse -Force $Env:AZP_WORK;
{{- end }}
    env:
      - name: CHART_VERSION
        value: "{{ .Chart.Version }}"
      - name: AGENT_DIAGLOGPATH
        value: C:\app-root\azp-logs
      - name: VSO_AGENT_IGNORE
        value: AZP_TOKEN
      - name: AZP_AGENT_NAME
        valueFrom:
          fieldRef:
            fieldPath: metadata.name
      - name: AZP_URL
        valueFrom:
          secretKeyRef:
            name: {{ include "common.secrets.name" (dict "existingSecret" .Values.secret.existingSecret "context" $) }}
            key: organizationURL
      - name: AZP_POOL
        value: {{ .Values.pipelines.poolName | quote | required "A value for .Values.pipelines.poolName is required" }}
      - name: AZP_TOKEN
        valueFrom:
          secretKeyRef:
            name: {{ include "common.secrets.name" (dict "existingSecret" .Values.secret.existingSecret "context" $) }}
            key: personalAccessToken
      {{- range .Values.pipelines.capabilities }}
      - name: {{ . }}
      {{- end }}
      {{- with .Values.extraEnv }}
      {{- toYaml . | nindent 6 }}
      {{- end }}
    resources:
      {{- toYaml .Values.windows.resources | nindent 6 | required "A value for .Values.resources is required" }}
    volumeMounts:
      - name: scripts-volume
        mountPath: C:\\app-root\\scripts
      - name: containerd-named-pipe
        mountPath: \\.\pipe\containerd-containerd
      - name: azp-logs
        mountPath: C:\\app-root\\azp-logs
      - name: azp-work
        mountPath: C:\\app-root\\azp-work
      {{- with .Values.extraVolumeMounts }}
      {{- toYaml . | nindent 6 }}
      {{- end }}
volumes:
  - name: scripts-volume
    configMap:
      name: {{ include "common.names.fullname" . }}-scripts
      defaultMode: 0755
  - name: containerd-named-pipe
    hostPath:
      path: \\.\pipe\containerd-containerd
      type: null
  - name: azp-logs
    emptyDir:
      sizeLimit: 1Gi
  - name: azp-work
    {{- if .Values.pipelines.cache.enabled }}
    ephemeral:
      volumeClaimTemplate:
        spec:
          accessModes: [ "ReadWriteOnce" ]
          storageClassName: {{ .Values.pipelines.cache.type | required "A value for .Values.pipelines.cache.type is required" }}
          resources:
            requests:
              storage: {{ .Values.pipelines.cache.size | required "A value for .Values.pipelines.cache.size is required" }}
    {{- else }}
    emptyDir:
      sizeLimit: {{ .Values.pipelines.cache.size | required "A value for .Values.pipelines.cache.size is required" }}
    {{- end }}
  {{- with .Values.extraVolumes }}
  {{- toYaml . | nindent 2 }}
  {{- end }}
nodeSelector:
  kubernetes.io/os: windows
  kubernetes.azure.com/mode: user
  {{- with .Values.windows.extraNodeSelectors }}
  {{- toYaml . | nindent 2 }}
  {{- end }}
{{- with .Values.windows.affinity }}
affinity:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.windows.tolerations }}
tolerations:
  {{- toYaml . | nindent 2 }}
{{- end }}