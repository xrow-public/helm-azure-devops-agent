{{ include "common.images.renderPullSecrets" ( dict "images" (list .Values.linux.image) "context" $) }}

serviceAccountName: {{ include "common.names.fullname" . }}
automountServiceAccountToken: false
{{- if .Values.linux.automountServiceAccountToken }}
automountServiceAccountToken: true
{{- else }}
automountServiceAccountToken: false
{{- end }}
initContainers: {{ toYaml .Values.linux.initContainers | nindent 2 }}
terminationGracePeriodSeconds: 30
restartPolicy: Never
securityContext: {{ toYaml .Values.linux.podSecurityContext | nindent 2 }}
containers:
  {{- if .Values.linux.sidecarContainers }}
  {{- toYaml .Values.linux.sidecarContainers | trim | nindent 2 }}
  {{- end }}
  - name: azp-agent
    securityContext: {{ toYaml .Values.linux.securityContext | nindent 6 }}
    image: {{ include "common.images.image" ( dict "imageRoot" .Values.linux.image "global" .Values.global ) }}
    imagePullPolicy: {{ .Values.linux.image.pullPolicy }}
    command:
      - /bin/bash
      - -c
    args:
      - |
        /app-root/scripts/start.sh
    startupProbe:
      failureThreshold: 1000
      periodSeconds: 10
      successThreshold: 1
      exec:
        command:
          - bash
          - -c
          - |
            if [[ "$(cat /app-root/azp-logs/*.log | grep 'Running job')" != "" ]]; then
              echo "Agent is running jobs";
              exit 0;
            fi
            echo "Agent is not running jobs";
            exit 1;
    livenessProbe:
      exec:
        command:
          - bash
          - -c
          - |
            if [[ ! -f /app-root/azp-agent/.agent ]]; then
              echo "Agent is not running";
              sleep 3;
              exit 1;
            fi
            exit 0;
      initialDelaySeconds: 10
      periodSeconds: 10
    lifecycle:
      preStop:
        exec:
          command:
            - bash
            - -c
            - |
{{- if or (not .Values.pipelines.cache.volumeEnabled) (not .Values.pipelines.tmpdir.volumeEnabled)}}
              {{- if not .Values.pipelines.cache.volumeEnabled }}
              # For security reasons, force clean the pipeline workspace at restart -- Sharing data bewteen pipelines is a security risk
              rm -rf ${AZP_WORK};
              {{- end }}
              {{- if not .Values.pipelines.tmpdir.volumeEnabled }}
              # For security reasons, force clean the tmpdir at restart -- Sharing data bewteen pipelines is a security risk
              rm -rf ${TMPDIR};
              {{- end }}
{{- end }}
    env:
      - name: CHART_VERSION
        value: "{{ .Chart.Version }}"
      - name: BUILDKIT_HOST
        value: unix:///run/user/0/buildkit/buildkitd.sock
      - name: AGENT_DIAGLOGPATH
        value: /app-root/azp-logs
      - name: VSO_AGENT_IGNORE
        value: AZP_TOKEN
      - name: AGENT_ALLOW_RUNASROOT
        value: "1"
      - name: AZP_AGENT_NAME
        valueFrom:
          fieldRef:
            fieldPath: metadata.name
      - name: AZP_URL
        valueFrom:
          secretKeyRef:
            name: {{ include "common.secrets.name" (dict "existingSecret" .Values.secret.existingSecret "context" $) }}
            key: organizationURL
      - name: AZP_POOL
        value: {{ .Values.pipelines.poolName | quote | required "A value for .Values.pipelines.poolName is required" }}
      - name: AZP_TOKEN
        valueFrom:
          secretKeyRef:
            name: {{ include "common.secrets.name" (dict "existingSecret" .Values.secret.existingSecret "context" $) }}
            key: personalAccessToken
      {{- range .Values.pipelines.capabilities }}
      - name: {{ . }}
      {{- end }}
      {{- with .Values.extraEnv }}
      {{- toYaml . | nindent 6 }}
      {{- end }}
    resources:
      {{- toYaml .Values.linux.resources | nindent 6 | required "A value for .Values.resources is required" }}
    volumeMounts:
      - mountPath: /app-root/scripts
        name: scripts-volume
      - mountPath: /var/lib/containerd
        name: containerd-volume
      - mountPath: /run/containerd/containerd.sock
        name: containerd-sock
      - name: azp-logs
        mountPath: /app-root/azp-logs
      - name: azp-work
        mountPath: /app-root/azp-work
      - name: local-tmp
        mountPath: /app-root/.local/tmp
      {{- with .Values.extraVolumeMounts }}
      {{- toYaml . | nindent 6 }}
      {{- end }}
volumes:
  - name: scripts-volume
    configMap:
      name: {{ include "common.names.fullname" . }}-scripts
      defaultMode: 0755
  - name: containerd-sock
    hostPath:
      path: /run/containerd/containerd.sock
  - name: containerd-volume
    hostPath:
      path: /var/lib/containerd
  - name: azp-logs
    emptyDir:
      sizeLimit: 1Gi
  - name: azp-work
    {{- if .Values.pipelines.cache.enabled }}
    ephemeral:
      volumeClaimTemplate:
        spec:
          accessModes: [ "ReadWriteOnce" ]
          storageClassName: {{ .Values.pipelines.cache.type | required "A value for .Values.pipelines.cache.type is required" }}
          resources:
            requests:
              storage: {{ .Values.pipelines.cache.size | required "A value for .Values.pipelines.cache.size is required" }}
    {{- else }}
    emptyDir:
      sizeLimit: {{ .Values.pipelines.cache.size | required "A value for .Values.pipelines.cache.size is required" }}
    {{- end }}
  - name: local-tmp
    {{- if .Values.pipelines.tmpdir.enabled }}
    ephemeral:
      volumeClaimTemplate:
        spec:
          accessModes: [ "ReadWriteOnce" ]
          storageClassName: {{ .Values.pipelines.tmpdir.type | required "A value for .Values.pipelines.tmpdir.type is required" }}
          resources:
            requests:
              storage: {{ .Values.pipelines.tmpdir.size | required "A value for .Values.pipelines.tmpdir.size is required" }}
    {{- else }}
    emptyDir:
      sizeLimit: {{ .Values.pipelines.tmpdir.size | required "A value for .Values.pipelines.tmpdir.size is required" }}
    {{- end }}
  {{- with .Values.extraVolumes }}
  {{- toYaml . | nindent 2 }}
  {{- end }}
nodeSelector:
  kubernetes.io/os: linux
  kubernetes.azure.com/mode: user
  {{- with .Values.linux.extraNodeSelectors }}
  {{- toYaml . | nindent 2 }}
  {{- end }}
{{- with .Values.linux.affinity }}
affinity:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.linux.tolerations }}
tolerations:
  {{- toYaml . | nindent 2 }}
{{- end }}