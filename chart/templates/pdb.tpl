{{- if .Values.pdb.enabled -}}
---
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: {{ include "common.names.fullname" . }}
  labels: {{ include "common.labels.standard" . | nindent 4 }}
spec:
  selector:
    matchLabels: {{ include "common.labels.matchLabels" . | nindent 6 }}
  maxUnavailable: 0
{{- end }}