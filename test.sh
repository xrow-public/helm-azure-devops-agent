#!/bin/bash

export KUBECONFIG=/root/.kube/service
helm dep update ./chart
helm lint ./chart
helm upgrade --install test ./chart -f agent.yaml -n azure-devops-agent-test  --create-namespace
# helm upgrade --install test oci://registry.gitlab.com/xrow-public/helm-azure-devops-agent/charts/azure-devops-agent --version 0.10.0 -f agent.yaml -n azure-devops-agent-test  --create-namespace

